import './App.css';
import { Header } from './Header';
import { Login } from './Login';
import { UserList } from './UserList';
import { withRouter, Route, Switch } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Header />
      <Switch>
        <Route exact path="/" component={Login} />
        <Route path="/home" component={UserList} />
      </Switch>
    </div>
  );
}

export default withRouter(App);
