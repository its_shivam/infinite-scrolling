import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Messages } from './util';

export const Login = () => {
  const history = useHistory();
  console.log('🚀 ~ file: Form.js ~ line 7 ~ Form ~ history', history);
  const [form, formSetter] = useState({});
  const [error, errorSetter] = useState({});
  const handleSubmit = () => {
    const { username = '', password = '' } = form;
    if (!username || !password) {
      window.alert('Form Validation failed');
      return;
    }
    // const lSUsername = localStorage.getItem('username');
    // const lSpassword = localStorage.getItem('password');
    if (username === 'foo' && password === 'bar') {
      localStorage.setItem('isLoggedIn', true);
      history.push('/home');
    } else {
      window.alert('Wrong Credentials');
    }
  };
  const handleFormChange = (e) => {
    console.log('🚀 ~ file: Form.js ~ line 18 ~ handleFormChange ~ e', e);
    const formName = e.target.name;
    console.log(
      '🚀 ~ file: Form.js ~ line 7 ~ handleFormChange ~ formName',
      formName
    );
    const formValue = e.target.value;
    console.log('formValue>>>>>>>>>>>>>>>', formValue);
    if (!formValue) {
      console.log('Inside this >>>>');
      errorSetter({ ...error, [formName]: true });
    } else {
      errorSetter({ ...error, [formName]: false });
    }
    console.log(
      '🚀 ~ file: Form.js ~ line 9 ~ handleFormChange ~ formValue',
      formValue
    );
    formSetter({ ...form, [formName]: formValue });
  };
  console.log('error>>>', error);
  return (
    <div style={{ width: '25%', margin: '0 auto', paddingTop: 50 }}>
      <div className="form-group">
        <label for="username">Email address</label>
        <input
          type="username"
          name="username"
          className="form-control"
          id="username"
          aria-describedby="username"
          placeholder="Enter Username"
          value={form.username}
          onChange={handleFormChange}
          style={error.username ? { borderColor: 'red' } : null}
        />
        {error.username && (
          <div className="errorText">{Messages.EMPTY_FIELD}</div>
        )}
      </div>
      <div className="form-group">
        <label for="password">Password</label>
        <input
          type="password"
          className="form-control"
          style={error.password ? { borderColor: 'red' } : null}
          id="password"
          name="password"
          placeholder="Password"
          value={form.password}
          onChange={handleFormChange}
        />
        {error.password && (
          <div className="errorText">{Messages.EMPTY_FIELD}</div>
        )}
      </div>
      <button type="submit" onClick={handleSubmit} className="btn btn-primary">
        Submit
      </button>
    </div>
  );
};
