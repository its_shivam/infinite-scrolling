/* eslint-disable no-unused-expressions */
/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from 'react';
// import $ from 'jquery';
import { useHistory } from 'react-router-dom';
import { Loader } from './Loader';
export const UserList = () => {
  const history = useHistory();
  const isLoggedIn = localStorage.getItem('isLoggedIn');
  if (!isLoggedIn) {
    history.push('/Login');
  }
  const [isLoading, loadSetter] = useState(false);
  const [userData, userDataSetter] = useState([]);
  const [counter, couterSetter] = useState(1);
  const fetchData = () => {
    loadSetter(true);
    var url = new URL(`https://reqres.in/api/users?page=${counter}`);
    fetch(url, {
      method: 'GET', // *GET, POST, PUT, DELETE, etc.
    })
      .then((res) => res.json())
      .then(
        (result) => {
          couterSetter(counter + 1);
          console.log(
            '🚀 ~ file: UserList.js ~ line 18 ~ fetchData ~ result',
            result
          );
          loadSetter(false);
          userDataSetter([...userData, ...result.data]);
        },
        (error) => {
          loadSetter(false);
        }
      );
  };

  const isScrolling = () => {
    if (
      window.innerHeight + document.documentElement.scrollTop !==
      document.documentElement.offsetHeight
    ) {
      return;
    }
    loadSetter(true);
  };

  useEffect(() => {
    fetchData();
    window.addEventListener('scroll', isScrolling);
    return () => window.removeEventListener('scroll', isScrolling);
  }, []);

  useEffect(() => {
    if (isLoading) {
      setTimeout(() => {
        fetchData();
      }, 10000);
    }
  }, [isLoading]);

  return (
    <div>
      {isLoading && <Loader />}
      <table style={{ overflow: 'scroll', margin: '0 auto' }}>
        <thead>
          <tr>
            <th> Contact Name </th>
            <th> Contact Image </th>
          </tr>
        </thead>
        <tbody>
          {userData.map((data) => {
            return (
              <tr>
                <td>
                  {data.first_name} {data.last_name}
                </td>
                <td>
                  <img src={data.avatar} alt="contact" />
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};
