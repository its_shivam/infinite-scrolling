export const Header = () => {
  return (
    <div
      style={{
        width: '100%',
        height: '20%',
        padding: '2%',
        backgroundColor: 'black',
        color: 'white',
      }}
    >
      <span style={{ cursor: 'pointer' }}> Contact Listing Application </span>
    </div>
  );
};
